<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

Route::get('/owner', function () {
    return view('admin.owner');
});

Route::get('/house', function () {
    return view('admin.house');
});

Route::get('/room', function () {
    return view('admin.room');
});
