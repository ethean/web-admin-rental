@extends('layouts.master')

@section('title')
   Dashborad
@stop

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
            <thead class=" text-primary">
                <th> Name</th>
                <th>Country</th>
                <th>City</th>
                <th class="text-right"> Salary</th>
            </thead>
            <tbody>
                <tr>
                <td> Dakota Rice</td>
                <td> Niger</td>
                <td> Oud-Turnhout </td>
                <td class="text-right">$36,738 </td>
                </tr>
                <tr>
                <td> Minerva Hooper</td>
                <td>Curaçao</td>
                <td> Sinaai-Waas</td>
                <td class="text-right">$23,789</td>
                </tr>
                <tr>
                <td>Sage Rodriguez</td>
                <td>Netherlands</td>
                <td>Baileux</td>
                <td class="text-right">$56,142</td>
                </tr>
                <tr>
                <td>Philip Chaney</td>
                <td> Korea, South</td>
                <td>Overland Park</td>
                <td class="text-right">$38,735</td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
@stop
