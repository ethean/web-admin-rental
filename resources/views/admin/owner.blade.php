@extends('layouts.master')

@section('title')
   Owner
@stop

@section('content')
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title " id="exampleModalLabel">Create Owner</h5>
                </button>
            </div>
            <div class="modal-body">
                <form class="/save-owner" method="POST">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nation Card ID:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">First Name:</label>
                        <input class="form-control" id="message-text"></textarea>
                        <label for="message-text" class="col-form-label">Last Name:</label>
                        <input class="form-control" id="message-text"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Gender:</label>
                        <input class="form-control" id="message-text"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Tel:</label>
                        <input class="form-control" id="message-text"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Owner
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Create</button>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th> ID </th>
                                <th> Username </th>
                                <th> Gender </th>
                                <th> Tel </th>
                                <th class="w-25"> Action </th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> 12 </td>
                                    <td> YangEthean </td>
                                    <td> M </td>
                                    <td> 1234 </td>
                                    <td clas>
                                        <button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 13 </td>
                                    <td> Ek Hout </td>
                                    <td> F </td>
                                    <td> 12345 </td>
                                    <td>
                                        <button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop
