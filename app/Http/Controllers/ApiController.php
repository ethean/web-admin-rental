<?php

namespace App\Http\Controllers;

use App\Owner;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function create(Request $request)
    {
        $owners = new Owner();

        $owners->cardid = $request->input('cardid');
        $owners->fname = $request->input('fname');
        $owners->lname = $request->input('lname');
        $owners->gender = $request->input('gender');
        $owners->tel = $request->input('tel');

        $owners->save();
        return response()->json($owners);
    }

    public function show()
    {
        $owners = Owner::all();
        return response()->json($owners);
    }

    public function showbyid($id)
    {
        $owners = Owner::find($id);
        return response()->json($owners);
    }

    public function updatebyid(Request $request, $id)
    {
        $owners = Owner::find($id);
        $owners->cardid = $request->input('cardid');
        $owners->fname = $request->input('fname');
        $owners->lname = $request->input('lname');
        $owners->gender = $request->input('gender');
        $owners->tel = $request->input('tel');

        $owners->save();
        return response()->json($owners);
    }

    public function deletebyid(Request $request, $id)
    {
        $owners = Owner::find($id);
        $owners->delete();
        return response()->json($owners);
    }
}
